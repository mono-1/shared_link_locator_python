#!/usr/bin/python

from boxsdk import JWTAuth
from boxsdk import Client
# from boxsdk import LoggingClient as Client
from boxsdk.object.events import EnterpriseEventsStreamType
import re, sys, dateutil.parser
from datetime import datetime, timedelta

import json

share_string_regex = r"([^\/]+$)"

appSettingsFile='YOUR_APPS_config.json'
sdk = JWTAuth.from_settings_file(appSettingsFile)
client = Client(sdk)

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

def get_target_share_id(arg):
    if 'http' in arg or 'box.com' in arg:
        return re.findall(share_string_regex, arg, re.IGNORECASE)[0]
    else:
        return arg

def check_events(id):
    before = datetime.now()
    after = before - timedelta(days=2)
    searching = True
    while searching:
        print("Searching for shared_link_id {}".format(id))
        print("Searching from {} back to {}".format(before, after))
        events = client.events().get_admin_events(limit=500, event_types=['SHARE'],created_before=before, created_after=after)
        print("Found {0} events.".format(events['chunk_size']))
        # for eventtype in events['entries']:
        #     print(eventtype.event_type)
        # print(dir(events))
        # print(events)
        if events['chunk_size'] > 499:
            print("Found too many links.")
            searching = False
        for event in events['entries']:
            # print(event.created_at, event.additional_details, event.source)
            if id == event.additional_details['shared_link_id']:
                print('Found {0}'.format(event.additional_details['shared_link_id']))
                return event
                # searching = False
                
        before = before - timedelta(days=2)
        after = after - timedelta(days=2)
        del events
        



target_share_id = get_target_share_id(sys.argv[1])
share_event = check_events(target_share_id)
print("#" * 44)
print("#" * 14 + color.BOLD + "  Match Found!  " + color.END + "#" * 14)
print("#" * 44)
print("{} \"{}\" shared by {} on {}.".format(
    share_event.source['item_type'].title(),
    share_event.source['item_name'],
    share_event.created_by['name'],
    share_event.created_at)
    )

print("This link is visible to {}".format(share_event.additional_details['sharedLinkSettings']['newVisibilityStatus']))
