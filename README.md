# Shared Link Locator - Python

Shared Link Locator is a Python script for recursively searching through a Box admin event log stream for a specific shared link.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip3 install -r requirements.txt
```

## Usage

```bash
python3 sharedLinkLocator.py [URL or shareID]
```